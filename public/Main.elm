import Markdown


main =
  Markdown.toHtml [] markdown


markdown = """

# Welcome to Open-Core 

Open-Core is an organization dedicated to bringing together people who share a
love for growing and sharing software

Our goals are to:
  * Help people grow their love, passion and skills in software development
  * Share information between ourselves so that we may continue learning as much
  as we can about software from each other
  * Bring people together to grow software of their own
  * Share our ideologies with the rest of the world

"""
















